package com.example.tic_tac_toe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import kotlinx.android.synthetic.main.activity_tic_tac_toe.*

class TicTacToeActivity : AppCompatActivity() {

    private var isPLayerOne: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tic_tac_toe)
        init()
    }

    private fun init() {
        button00.setOnClickListener() {
            changeButton(button00)
        }

        button01.setOnClickListener() {
            changeButton(button01)
        }

        button02.setOnClickListener() {
            changeButton(button02)
        }

        button10.setOnClickListener() {
            changeButton(button10)
        }

        button11.setOnClickListener() {
            changeButton(button11)
        }

        button12.setOnClickListener() {
            changeButton(button12)
        }

        button20.setOnClickListener() {
            changeButton(button20)
        }

        button21.setOnClickListener() {
            changeButton(button21)
        }

        button22.setOnClickListener() {
            changeButton(button22)
        }

        reset.setOnClickListener() {
            buttonContainer.visibility = View.VISIBLE
            resultTextView.text = ""
            isPLayerOne = true
            button00.isClickable = true
            button00.text = ""
            button01.isClickable = true
            button01.text = ""
            button02.isClickable = true
            button02.text = ""
            button10.isClickable = true
            button10.text = ""
            button11.isClickable = true
            button11.text = ""
            button12.isClickable = true
            button12.text = ""
            button20.isClickable = true
            button20.text = ""
            button21.isClickable = true
            button21.text = ""
            button22.isClickable = true
            button22.text = ""
        }
    }

    private fun changeButton(button: Button) {
        d("Click", "button")
        if (isPLayerOne) {
            button.text = "X"
        } else {
            button.text = "O"
        }
        button.isClickable = false
        isPLayerOne = !isPLayerOne
        checkWinner()

    }


    private fun deleteClick() {
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button10.isClickable = false
        button11.isClickable = false
        button12.isClickable = false
        button20.isClickable = false
        button21.isClickable = false
        button22.isClickable = false
    }


    private fun checkWinner() {
        if (button00.text.isNotEmpty() && button00.text.toString() == button01.text.toString() && button00.text.toString() == button02.text.toString()) {
            resultTextView.text = "Winner is ${button00.text}"
            deleteClick()
        } else if (button10.text.isNotEmpty() && button10.text.toString() == button11.text.toString() && button10.text.toString() == button12.text.toString()) {
            resultTextView.text == "Winner is ${button10.text}"
            deleteClick()
        } else if (button20.text.isNotEmpty() && button20.text.toString() == button21.text.toString() && button20.text.toString() == button22.text.toString()) {
            resultTextView.text = "Winner is ${button20.text}"
            deleteClick()
        } else if (button00.text.isNotEmpty() && button00.text.toString() == button10.text.toString() && button00.text.toString() == button20.text.toString()) {
            resultTextView.text = "Winner is ${button00.text}"
            deleteClick()
        } else if (button01.text.isNotEmpty() && button01.text.toString() == button11.text.toString() && button01.text.toString() == button21.text.toString()) {
            resultTextView.text = "Winner is ${button01.text}"
            deleteClick()
        } else if (button02.text.isNotEmpty() && button02.text.toString() == button12.text.toString() && button02.text.toString() == button22.text.toString()) {
            resultTextView.text = "Winner is ${button02.text}"
            deleteClick()
        } else if (button00.text.isNotEmpty() && button00.text.toString() == button11.text.toString() && button00.text.toString() == button22.text.toString()) {
            resultTextView.text = "Winner is ${button00.text}"
            deleteClick()
        } else if (button02.text.isNotEmpty() && button02.text.toString() == button11.text.toString() && button02.text.toString() == button20.text.toString()) {
            resultTextView.text = "Winner is ${button02.text}"
            deleteClick()
        } else if (button00.text.isNotEmpty() && button01.text.isNotEmpty() && button02.text.isNotEmpty() && button10.text.isNotEmpty() && button11.text.isNotEmpty()
            && button12.text.isNotEmpty() && button20.text.isNotEmpty() && button21.text.isNotEmpty() && button22.text.isNotEmpty()
        ) {
            resultTextView.text = "Draw"
            deleteClick()
        }

    }
}



